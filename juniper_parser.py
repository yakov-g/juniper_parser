import argparse
import json
import time
import collections
import os
import threading
import multiprocessing
import mmap
from subprocess import call

counters = {}

manager = multiprocessing.Manager()

global_json_list = manager.list()
counters["records"] = manager.dict()
counters["timestamps"] = manager.dict()

counters["sys_ids"] = manager.dict()
counters["comp_ids"] = manager.dict()
counters["attributes"] = manager.list()
#counters["prefixes"] = manager.dict()
#counters["unique_keys"] = manager.dict()

#counters["sys_ids"] = dict()
#counters["comp_ids"] = dict()
#counters["attributes"] = dict()
counters["prefixes"] = dict()

num = manager.Value(int, 0)

csv_headers = ["timestamp","component_id","sequence_number"]
#csv_headers = ["timestamp","component_id","sequence_number", "path"]
#csv_headers = []

lock = multiprocessing.Lock()

def value_cast(value, type_id):
    if type_id == "str_value":
        pass
    elif type_id == "uint_value":
        value = int(value)
    elif type_id == "int_value":
        value = int(value)
    elif type_id == "double_value":
        value = float(value)
    elif type_id == "bool_value":
        if value == "True":
            value = True
        elif value == "False":
            value = False
        else:
            pass
    else:
        print("Type is not defined: {}".format(type_id))
        exit(1)
    return value

class Record:
    def __init__(self):
        self.kv = "kv"
        self._data = collections.OrderedDict()

    def kv_add(self, k, v):
        if self.kv not in self._data:
            self._data[self.kv] = collections.OrderedDict()

        if k in self._data[self.kv]:
            print("kv_add: key '{}' is already in record".format(k))
            return
        self._data[self.kv][k] = v

    def add(self, k, v):
        if k in self._data:
            print("{} is already in record".format(k))
            return
        self._data[k] = v


    def prefix_append(self, d):
        key = "prefixes"
        if key not in self._data:
            self._data[key] = collections.OrderedDict()
        self._data[key][d.name_get()] = d.data_get()

        global counters
        counters["prefixes"][d.name_get()] = 1

    def data_get(self):
        return self._data

    def _csv_header_get(self):
        ret = ""
        for h in csv_headers:
            ret += str(self._data[h]) + ","
        ret += str(self._data[self.kv]["__timestamp__"])
        return ret

    def csv_str_get(self, attr_list):
        ret = ""
        if "sensor_1002" not in self._data["path"]:
            return ret
        common_s = self._csv_header_get()

        if "prefixes" in self._data:
            prefixes = self._data["prefixes"]
            for name, data in prefixes.items():

                if "parent-ae-name" not in data:
                    continue
                """
                if data["parent-ae-name"] != "ae24.366":
                    continue
                """
                if "xe-11/0/20" not in  data["__prefix__"]:
                    continue

                pref_s = common_s
                for a in attr_list:
                    pref_s += ","
                    if a in data:
                        if a == "__prefix__":
                            val = ""
                        else:
                            val = data[a]

                        if val == "up":
                            val = 1
                        elif val == "down":
                            val = 0
                        pref_s += str(val)
                    else:
                        pref_s += str(0)
                        pass
                ret += (pref_s + "\n")

        ret = ret[:-1]
        return ret

class Device:
    def __init__(self, name):
        self._data = collections.OrderedDict()
        #self.kv = "kv"
        self._name = name
        #self._data = {self.kv : {}}

    """
    def kv_add(self, k, v):
        if k in self._data[self.kv]:
            print("kv_add: key '{}' is already in record".format(k))
            return
        self._data[self.kv][k] = v
    """

    def add(self, k, v):
        if k in self._data:
            print("{} is already in record".format(k))
            return
        self._data[k] = v

    def name_get(self):
        return self._name

    def data_get(self):
        return self._data


def process_record_lines(lines, thread_num, ae_dict, attr_set):
    in_kv = False
    kv_start = "kv {"
    kv_end = "}"
    kv_lines = []
    d = None
    r = Record()
    #prefix
    i = 0

    ts = ""
    seq = ""
    path = ""
    comp = ""
    for l in lines:
        if kv_start in l:
            break
        i += 1
        k = l.split(":", 1)
        key = k[0]
        value = k[1]

        if key == "system_id":
            """
            if k[1] not in counters["sys_ids"]:
                counters["sys_ids"][k[1]] = 1
            """
            pass

        elif key == "component_id":
            """
            if k[1] not in counters["comp_ids"]:
                counters["comp_ids"][k[1]] = 1
            """
            comp = value
            pass
        elif key == "timestamp":
            value = value_cast(value, "uint_value")
            ts = value
        elif k[0] == "sequence_number":
            value = value_cast(value, "uint_value")
            seq = value
        elif k[0] == "path":
            path = value

        r.add(key, value)


    tup = (ts, seq, path, comp)
    ret = False
    #lock.acquire()
    if tup not in counters["timestamps"]:
        counters["timestamps"][tup] = 1
    else:
        ret = True
    #lock.release()
    if ret:
        return None

    lines = lines[i:]

    d = None
    for l in lines:
        if kv_start in l:
            in_kv = True
        elif kv_end in l:
            in_kv = False
            if len(kv_lines) != 2:
                print("kv should have 2 lines: {}".format(kv))
                exit(1)
            key = kv_lines[0].split(":", 1)[1]
            l2 = kv_lines[1].split(":", 1)

            type_id = l2[0]
            val = l2[1]
            val = value_cast(val, type_id)

            #counters["unique_keys"][key] = 1

            # count number of AE occurances
            """

                if val not in ae_dict:
                    ae_dict[val] = 0
                ae_dict[val] += 1
            """


            if key == "__prefix__":
                if d:
                    r.prefix_append(d)
                    del d

                d = Device(val)

            if d:
                d.add(key, val)
                #attr_set.add(key)
                """
                if key == "parent_ae_name" or key == "parent-ae-name":
                    lock.acquire()
                    if val == "ae24.366":
                        num.value += 1
                    lock.release()
                """
            else:
                r.kv_add(key, val)

            kv_lines = []
        else:
            if in_kv:
                kv_lines.append(l)
    if d:
        r.prefix_append(d)
        del d

    return r


def parse(in_file, mmap_obj, out_file, n_records, data, frmt):
    thread_num = data
    counters["records"][str(thread_num)] = 0
    counters["records"][str(thread_num) + "w"] = 0
    ae_dict = {}
    attributes = set()
    json_list = []
    attr_list = list(counters["attributes"])

    stop_token = "system_id"
    lines = []
    i = 0

    if out_file:
        outfile = open(out_file + str(thread_num), 'w')

    if out_file and frmt == "csv" and thread_num == 0:
        s = ""
        for h in csv_headers:
            s += h + ","
        s += "__timestamp__"
        for a in attr_list:
            s += ("," + a)
        outfile.write(s+"\n")

    if mmap_obj:
        for line in iter(mmap_obj.readline, ""):
            if stop_token not in line:
                continue
            line = line.strip()
            lines.append(line)
            break

        for line in iter(mmap_obj.readline, ""):
            line = line.strip()
            if len(line) == 0:
                continue
            if stop_token in line:
                r = process_record_lines(lines, thread_num, ae_dict, attributes)
                #exit thread
                if r is None:
                    break

                if out_file:
                    #json_list.append(r.data_get())
                    if frmt == "json":
                        json.dump(r.data_get(), outfile, indent=3)
                        outfile.write(",\n")
                    elif frmt == "csv":
                        l = r.csv_str_get(attr_list)
                        if len(l):
                            outfile.write(l+"\n")
                del r

                i += 1
                counters["records"][str(thread_num)] += 1
                lines = []

            if i == n_records:
                break
            lines.append(line)


        if len(lines):
            r = process_record_lines(lines, thread_num, ae_dict, attributes)
            if r:
                i += 1
                counters["records"][str(thread_num)] += 1
                if out_file:
                    #json_list.append(r.data_get())
                    if frmt == "json":
                        json.dump(r.data_get(), outfile, indent=3)
                        outfile.write(",\n")
                    elif frmt == "csv":
                        l = r.csv_str_get(attr_list)
                        if len(l):
                            outfile.write(l+"\n")
                del r

    #print("Tread {}: {} records parsed".format(thread_num, i))

    """
    if out_file:
        #lock.acquire()
        print("Thread: {} Start copying into a file. List has obj: {}".format(thread_num, len(json_list)))
        #with open(out_file + str(thread_num), 'a') as outfile:
        for l in json_list:
            json.dump(l, outfile, indent=3)
            outfile.write(",\n")
            counters["records"][str(thread_num) + "w"] += 1
        print("Thread: {} Copied into a file: {}".format(thread_num, counters["records"][str(thread_num) + "w"]))
        #lock.release()
    del json_list
    """


    """
    for k, v in ae_dict.items():
        if k not in counters["ae"]:
            counters["ae"][k] = v
        else:
            counters["ae"][k] += v
    """

    """
    lock.acquire()
    for a in attributes:
        counters["attributes"].append(a)
    lock.release()
    """
if __name__ == '__main__':
    st = time.time()
    parser = argparse.ArgumentParser()

    parser.add_argument("-f", "--in_file", required=True,
                        help="input file")
    parser.add_argument("-o", "--out_file",
                        help="ouput file")
    parser.add_argument("-a", "--attr_file",
                        help="file with list of attributes (required for csv output)")
    parser.add_argument("-n", "--num", type=int, default=1,
                        help="number of records to parse (-1 for all)")
    parser.add_argument("-t", "--threads", type=int, default=1,
                        help="number of threads (use > 1 thread only when parsing all file)")
    parser.add_argument('--format', default="json", choices=["json", "csv"],
                        help="output format")

    options = parser.parse_args()

    in_file = options.in_file
    out_file = options.out_file
    attr_file = options.attr_file
    n_records = options.num
    n_threads = options.threads
    frmt = options.format

    if frmt == "csv" and attr_file is None:
        print("Error: provide file with attributes when generating csv")
        exit(0)

    if n_records != -1 and n_threads > 1:
        print("Error: when specifying number of records, use only one thread")
        exit(0)

    if attr_file:
        with open(attr_file, "r") as attrfile:
            for line in attrfile:
                line = line.strip()
                counters["attributes"].append(line)

    sz = os.path.getsize(in_file)
    sz_per_thread = sz / n_threads

    mmap_object = None
    f = open(in_file, 'r')

    threads = []
    for i in range(n_threads):
        offset_start = int((i * sz_per_thread) / mmap.PAGESIZE) * mmap.PAGESIZE

        length = 0
        if n_threads > 1 and i != n_threads -1:
            length = sz_per_thread + 100000
        mmap_object = mmap.mmap(f.fileno(), length=length, prot = mmap.PROT_READ, offset=offset_start)
        t = multiprocessing.Process(target=parse, args = (in_file, mmap_object, out_file, n_records, i, frmt))
        t.start()
        threads.append(t)

    for t in threads:
        t.join()

    rec_sum = 0
    for k, v in counters["records"].items():
        if "w" not in k:
            print("Thread {} parsed {};".format(k, v))
            rec_sum += v
    print("Records {}".format(rec_sum))

    if out_file:
        for i in range(1, n_threads):
            f_out = out_file + "0"
            f_in = out_file + str(i)
            print("Concat files: {} {}".format(f_in, f_out))
            cmd = "cat {} >> {}".format(f_in, f_out)
            call(cmd, shell=True)
            os.remove(f_in)

        os.rename(out_file + "0", out_file)


    et = time.time()
    print("sys_ids count: {}".format(len(counters["sys_ids"])))
    print(counters["sys_ids"])
    print("comp_ids count: {}".format(len(counters["comp_ids"])))
    print(counters["comp_ids"])


    """
    attr_set = set(counters["attributes"])
    attributes = sorted(attr_set)
    print(len(attributes))
    for a in attributes:
        print a
    """
    """
    print("=========== prefixes count: {}".format(len(prefixes)))
    prefixes = sorted(prefixes)
    for a in prefixes:
        print a
    print("=========== unique keys count: {}".format(len(unique_keys)))
    unique_keys = sorted(unique_keys)
    for a in unique_keys:
        print a
    """

    """
    json_dict = {"data" : list(global_json_list)}
    if out_file:
        with open(out_file, 'w') as outfile:
            json.dump(json_dict, outfile, indent=3)
    """

    print("Run time: {}".format(et - st))
    #print("AE count: {}".format(num.value))
