#!/usr/bin/env python
import csv
import sys
import pprint
import argparse
import json
import time
import collections
import os
import threading
import multiprocessing
import mmap
import matplotlib.pyplot as plt
from subprocess import call

 # Function to convert a csv file to a list of dictionaries.  Takes in one variable called "variables_file"

class Parser:
    def __init__(self):
        self._dict_list = []
        pass

    def file_read(self, filename):
        reader = csv.DictReader(open(filename, 'rb'))
        for line in reader:
            self._dict_list.append(line)

    def data_print(self):
        pprint.pprint(self._dict_list)

    def request(self, key_fields_dict, ret_field_tup):
        if not isinstance(ret_field_tup, tuple):
            print("ret_field_tup must be a tuple")
            return None

        ret = tuple(list() for i in range(len(ret_field_tup)))
        for d in self._dict_list:
            key_found = True
            for key_field, key_value_list in key_fields_dict.items():
                if d[key_field] not in key_value_list:
                    key_found = False

            if key_found:
                for r, f in zip(ret, ret_field_tup):
                    r.append(float(d[f]))
        return ret

# Calls the csv_dict_list function, passing the named csv


# Prints the results nice and pretty like

if __name__ == '__main__':
    st = time.time()
    parser = argparse.ArgumentParser()

    parser.add_argument("-f", "--in_file", required=True,
                        help="input file")
    parser.add_argument("-o", "--out_file",
                        help="ouput file")
    parser.add_argument("-a", "--attr_file",
                        help="file with list of attributes (required for csv output)")
    parser.add_argument("-n", "--num", type=int, default=1,
                        help="number of records to parse (-1 for all)")
    parser.add_argument("-t", "--threads", type=int, default=1,
                        help="number of threads (use > 1 thread only when parsing all file)")
    parser.add_argument('--format', default="json", choices=["json", "csv"],
                        help="output format")

    options = parser.parse_args()

    in_file = options.in_file
    out_file = options.out_file
    attr_file = options.attr_file
    n_records = options.num
    n_threads = options.threads
    frmt = options.format

    p = Parser()
    p.file_read(in_file)
    """
    This API:
    p.request({"field1":["value1", "value2"], "field2":["value3", "value4"]}, ("field3", "field4"))
    equals to request:
    get ("field3", "field4") FROM TABLE where "field1" == ("value1" or "value2") and "field2" == ("value3" or "value4")
    """
    #ret = p.request({"parent-ae-name":["ae24.407", "ae24.406"], "counters/in-multicast-pkts":["0"]}, ("timestamp", "counters/in-unicast-pkts"))
    ret = p.request({"parent-ae-name":["ae24.407"]}, ("timestamp", "counters/in-unicast-pkts"))
    plt.plot(ret[0], ret[1])
    #print(ret)
    plt.show()

    et = time.time()
    print("Run time: {}".format(et - st))
